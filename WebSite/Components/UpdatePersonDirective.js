angular.module('SimpleCsv')
	.directive('updatePerson', function(){
        return {
             restrict: 'E',
                priority: 0,
                templateUrl: 'Components/UpdatePerson.html',
                controller: 'UpdatePersonController'
            }
        });