﻿using System;

namespace Qaits.Exceptions
{
    public class PhoneNumberUnableToReceiveSmsException : Exception
    {
        public PhoneNumberUnableToReceiveSmsException(string message) : base(message)
        {

        }
    }

    public class InvalidPhoneNumberException : Exception
    {
        public InvalidPhoneNumberException(string message)
            : base(message)
        {

        }
    }
}
