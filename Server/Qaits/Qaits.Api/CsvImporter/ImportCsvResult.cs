﻿namespace Qaits.Api.CsvImporter
{
    public class ImportCsvResult<TResult>
    {
        public TResult[] Results { get; protected set; }
        public InvalidImportResult[]  InvalidImportResults { get; protected set; }

        public ImportCsvResult(TResult[] results, InvalidImportResult[] invalidImportResults)
        {
            Results = results;
            InvalidImportResults = invalidImportResults;
        }
    }
}