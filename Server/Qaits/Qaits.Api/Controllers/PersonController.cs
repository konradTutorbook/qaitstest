﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Qaits.Api.CsvImporter;
using Qaits.Api.Validators;
using Qaits.Exceptions;
using Qaits.Persistence;

namespace Qaits.Api.Controllers
{
    public class PersonController : ApiController
    {
        private readonly EntityValidator entityValidator;

        public PersonController(EntityValidator entityValidator)
        {
            this.entityValidator = entityValidator;
        }

        [Route("api/Person")]
        public HttpResponseMessage GetPerson(int personId)
        {
            using (var context = new QaitsContext(Settings.GetConnectionString()))
            {
                var person = context.Person.SingleOrDefault(c => c.Id == personId);

                if (person == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new
                    {
                        Message = "No person was found."
                    });

                return Request.CreateResponse(HttpStatusCode.OK, person);
            }
        }

        [Route("api/Persons/{pageNumber:int=1}")]
        public HttpResponseMessage GetPersons(int pageNumber)
        {
            try
            {
                using (var context = new QaitsContext(Settings.GetConnectionString()))
                {
                    var orderedQuery = context.Person.OrderBy(c => c.Id)
                         .Skip(NumberOfRecordsToSkip(pageNumber, Settings.PageSize))
                         .Take(Settings.PageSize);

                    var persons = orderedQuery.ToList();

                    var totalResults = context.Person.OrderByDescending(c => c.Id)
                        .Count();

                    var pageCount = (int)(totalResults / Settings.PageSize);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Results = persons,
                        CurrentPage = pageNumber,
                        PageCount = pageCount,
                        RowCount = persons,
                        PageSize = Settings.PageSize,
                        TotalResults = totalResults,
                    });
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [Route("api/Person/UploadCsv")]
        public async Task<HttpResponseMessage> PostPersonUploadCsv()
        {
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                var csvImporter = new PersonCsvImporter(entityValidator);

                var result = await csvImporter.ImportCsv(Request.Content);

                using (var context = new QaitsContext(Settings.GetConnectionString()))
                {
                    context.Person.AddOrUpdate(result.Results);
                    context.SaveChanges();
                }

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e);
            }
        }
     
        [Route("api/Person")]
        public HttpResponseMessage PutPerson(Person person)
        {
            try
            {
                entityValidator.Validate(person);

                using (var context = new QaitsContext(Settings.GetConnectionString()))
                {
                    context.Person.AddOrUpdate(person);
                    context.SaveChanges();
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ValidationException validationException)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, validationException.ValidationResult);
            }
        }

        [Route("api/Person/{id}")]
        public HttpResponseMessage DeletePerson(int id)
        {
            using (var context = new QaitsContext(Settings.GetConnectionString()))
            {
                var person = context.Person.FirstOrDefault(c => c.Id == id);

                if (person != null)
                {
                    context.Person.Remove(person);
                    context.SaveChanges();
                }

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Message = "Person has been removed successfully."
                });
            }
        }

        protected int NumberOfRecordsToSkip(int pageNumber, int selectSize)
        {
            Mandate.ParameterCondition(pageNumber > 0, "pageNumber");
            int adjustedPageNumber = pageNumber - 1; //we adjust for the fact that sql server starts at page 0

            return selectSize * adjustedPageNumber;
        }
    }
}