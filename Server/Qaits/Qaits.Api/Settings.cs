﻿using System.Configuration;

namespace Qaits.Api
{
    public class Settings
    {
        private const string DatabaseName = "SimpleCsv";

        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[DatabaseName].ToString();
        }
        public const int PageSize = 10;

    }
}