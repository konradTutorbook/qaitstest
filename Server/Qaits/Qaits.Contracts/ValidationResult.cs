﻿namespace Qaits.Contracts
{
    public class ValidationResult
    {
        public string Field { get; private set; }
        public string Message { get; private set; }

        public ValidationResult(string field, string message)
        {
            Field = field;
            Message = message;
        }
    }
}