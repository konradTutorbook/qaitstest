﻿using System.Data.Entity;

namespace Qaits.Persistence
{
    public class QaitsContext : DbContext
    {
        public IDbSet<Person> Person { get; set; } 

        public QaitsContext(string databaseName)
            : base(databaseName)
        {
            
        }
    }
}
